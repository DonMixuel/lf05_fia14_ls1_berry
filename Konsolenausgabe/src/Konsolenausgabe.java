
public class Konsolenausgabe {

	public static void main(String[] args) {
		//Anfang des Beispielsatzes
		System.out.println("Das ist ein \"Beipsielsatz\".");
		System.out.println("Ein Beispielsatz ist das.");
		
		System.out.println("");
		
	    // Der unterschied liegt in der formatierug der verschiedenen strings
		System.out.println("       *\n      ***\n     *****\n    *******\n   *********\n  ***********\n *************\n      ***\n      ***\n");
		
		System.out.println("");
		System.out.println("");
		
		//nur 2.kommastelle
		System.out.printf("%.2f\n",22.4234234);
		System.out.printf("%.2f\n",111.2222);
		System.out.printf("%.2f\n",4.0);
		System.out.printf("%.2f\n",1000000.551);
		System.out.printf("%.2f\n",97.34);
		
		System.out.println("");
		
		System.out.printf("%6s\n", "**");
		System.out.printf("%1s", "*");
		System.out.printf("%9s\n", "*");
		System.out.printf("%1s", "*");
		System.out.printf("%9s\n", "*");
		System.out.printf("%6s\n", "**");
		
	//Erste Zeile
		System.out.printf("0!","%5s");
		System.out.printf("%5s","=");
		System.out.printf("%20s","=");
		System.out.printf("%5s","1\n");
	//Zweite Zeile
		System.out.printf("1!","%5s");
		System.out.printf("%5s","=");
		System.out.printf(" 1");
		System.out.printf("%18s","=");
		System.out.printf("%5s","1\n");	
	//Dritte Zeile	
		System.out.printf("2!","%5s");
		System.out.printf("%5s","=");
		System.out.printf(" 1 * 2");
		System.out.printf("%14s","=");
		System.out.printf("%5s","2\n");		
	//Vierte Zeile	
		System.out.printf("3!","%5s");
		System.out.printf("%5s","=");
		System.out.printf(" 1 * 2 * 3");
		System.out.printf("%10s","=");
		System.out.printf("%5s","6\n");	
	//F�nfte Zeile	
		System.out.printf("4!","%5s");
		System.out.printf("%5s","=");
		System.out.printf(" 1 * 2 * 3 * 4");
		System.out.printf("%6s","=");
		System.out.printf("%5s","24\n");
	
	//Sechste Zeile	
		System.out.printf("5!","%5s");
		System.out.printf("%5s","=");
		System.out.printf(" 1 * 2 * 3 * 4 * 5");
		System.out.printf("%2s","=");
		System.out.printf("%5s","120\n");	
	
		System.out.println("   ");
		
		
   //Fahrenheit + Celsius
		System.out.println(" Fahrenheit   |   Celsius");
		System.out.println("--------------------------");
		System.out.printf("-20","%5s");
		System.out.printf("%12s","|");
		System.out.printf("%10.2f\n", -28.8889 );
		//pause
		System.out.printf("-10","%5s");
		System.out.printf("%12s","|");
		System.out.printf("%10.2f\n", -23.3333 );
		//pause
		System.out.printf("+0","%5s");
		System.out.printf("%13s","|");
		System.out.printf("%10.2f\n", -17.7778 );
		//pause
		System.out.printf("+20","%5s");
		System.out.printf("%12s","|");
		System.out.printf("%10.2f\n", -6.6667  );
		//pause
		System.out.printf("+30","%5s");
		System.out.printf("%12s","|");
		System.out.printf("%10.2f\n", -1.1111 );
		//ende
		
		
			
		
		
	}
}
